require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:taxon_1)     { create :taxon }
    let!(:product_1)  { create :product, taxons: [taxon_1] }
    let!(:product_2)  { create :product, taxons: [taxon_1] }

    before :each do
      get :show, params: { id: product_1.id }
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq product_1
    end

    it 'assigns @taxons' do
      expect(assigns(:taxons)).to match_array taxon_1
    end

    it 'assigns @related_products' do
      expect(assigns(:related_products)).to match_array product_2
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end
end
