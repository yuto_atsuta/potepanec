require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:taxon) { create(:taxon) }
    let!(:taxonomy) { taxon.taxonomy }

    before :each do
      get :show, params: { id: taxon.id }
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it 'assigns @taxon' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'assigns @categories' do
      expect(assigns(:categories)).to match_array taxonomy
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end
end
