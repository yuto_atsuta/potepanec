require 'rails_helper'

describe "categoriesページ", type: :system do
  let(:taxonomy_1)  { create :taxonomy }
  let(:taxon_2)     { create :taxon,    name: 'Tops',   parent_id: taxonomy_1.root.id }
  let(:taxon_3)     { create :taxon,    name: 'Pants',  parent_id: taxonomy_1.root.id }
  let(:taxon_4)     { create :taxon,    name: 'Denim',  parent_id: taxon_3.id }
  let!(:product_1)  { create :product,  name: 'Polo-shirt',   taxons: [taxon_2] }
  let!(:product_2)  { create :product,  name: 'Knit',         taxons: [taxon_2] }
  let!(:product_3)  { create :product,  name: 'Skinny-denim', taxons: [taxon_4] }

  context "categories_showページが表示されている時" do
    before do
      visit potepan_category_path(taxon_4.id)
    end

    it "カテゴリー名が一覧にされ、productがあるカテゴリーは数字が入った括弧が表示されること" do
      within(".test-panel-body") do
        expect(page).to have_content "Tops(2)"
        expect(page).to have_content "Denim(1)"
        expect(page).not_to have_content "Pants("
      end
    end

    it "カテゴリー名をクリックするとそのカテゴリーページへ移動し、そのカテゴリーに属する商品と金額のみが表示されること" do
      click_on taxon_2.name
      expect(current_path).to eq potepan_category_path(taxon_2.id)
      within(".test-productBox") do
        expect(page).to have_content "Polo-shirt"
        expect(page).to have_content "Knit"
        expect(page).not_to have_content "Skinny-denim"
        expect(page).to have_content product_1.display_price
      end
    end
  end
end
