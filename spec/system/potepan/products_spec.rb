require 'rails_helper'

describe "productsページ", type: :system do
  let(:taxon_1)     { create :taxon,    name: 'Tops' }
  let(:taxon_2)     { create :taxon,    name: 'Pants' }
  let!(:product_1)  { create :product,  name: 'Polo-shirt',   taxons: [taxon_1] }
  let!(:product_2)  { create :product,  name: 'Knit',         taxons: [taxon_1] }
  let!(:product_3)  { create :product,  name: 'Skinny-denim', taxons: [taxon_2] }

  context "products_showページが表示されている時" do
    before do
      visit potepan_product_path(product_1.id)
    end

    it "プロダクトの詳細情報が表示されること" do
      within(".media") do
        expect(page).to have_content "Polo-shirt"
        expect(page).not_to have_content "Knit"
        expect(page).not_to have_content "Skinny-denim"
        expect(page).to have_content product_1.display_price
        expect(page).to have_content product_1.description
      end
    end

    it "関連プロダクトが表示されること" do
      within(".productBox") do
        expect(page).to have_content "Knit"
        expect(page).not_to have_content "Polo-shirt"
        expect(page).not_to have_content "Skinny-denim"
        expect(page).to have_content product_2.display_price
      end
    end

    it "関連プロダクトをクリックするとそのプロダクトページへ移動すること" do
      click_on product_2.name
      expect(current_path).to eq potepan_product_path(product_2.id)
    end
  end
end
