class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @taxons = @product.taxons.includes(products: [master: [:default_price, :images]])
    @related_products = @taxons.flat_map(&:products).uniq.reject { |product| product == @product }
  end
end
