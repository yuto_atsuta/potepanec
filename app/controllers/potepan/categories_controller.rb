class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.includes(products: [master: [:default_price, :images]]).find(params[:id])
    @categories = Spree::Taxonomy.all.eager_load(root: [children: :children])
  end
end
